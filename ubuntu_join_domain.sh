#!/bin/bash
set -e

# Re-run the script if not using sudo/root
function detect_current_uid()
{
  echo $(id -u)
};

function _rerun_script_as_root()
{
  if [ $(detect_current_uid) -ne "0" ]; then
    local ENVVARS=$(echo $@)
    echo "Re-running script as root..."
    exec sudo $ENVVARS bash $0
  fi
};

_rerun_script_as_root

read -p "Enter domain user e.g. username@domain.com: " full_user
user=${full_user%@*}
domain=${full_user#*@}
domain_upper=$(echo "$domain" | tr '[:lower:]' '[:upper:]')

#apt-get update \
#  && apt-get upgrade -y

apt-get update \
  && apt-get install -y realmd sssd samba-common samba-common-bin samba-libs sssd-tools krb5-user adcli packagekit vim

kinit -V $user
realm --verbose join -U $user $domain

echo "ad_hostname = $(hostname).$domain" >> /etc/sssd/sssd.conf
echo "dyndns_update = True" >> /etc/sssd/sssd.conf
systemctl restart sssd

# Domain Users get home directory
if [ -f /etc/pam.d/common-session ]; then
  echo 'session required pam_mkhomedir.so skel=/etc/skel/ umask=0022' >> /etc/pam.d/common-session
fi

# Allow Unity Sessions
if [ -f /usr/share/lightdm/lightdm.conf.d/50-ubuntu.conf ]; then
  echo 'greeter-show-manual-login=true' >> /usr/share/lightdm/lightdm.conf.d/50-ubuntu.conf
fi

if [ -f /usr/share/lightdm/lightdm.conf.d/50-ubuntu.conf ]; then
  echo 'greeter-hide-users=true' >> /usr/share/lightdm/lightdm.conf.d/50-ubuntu.conf
fi

if [ -f /usr/share/lightdm/lightdm.conf.d/50-ubuntu.conf ]; then
  echo 'allow-guest=false' >> /usr/share/lightdm/lightdm.conf.d/50-ubuntu.conf
fi

echo "%domain\ admins@$domain	ALL=(ALL:ALL) ALL" >> /etc/sudoers
echo "%linuxadmins@$domain	ALL=(ALL:ALL) ALL" >> /etc/sudoers